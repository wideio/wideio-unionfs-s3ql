
#!/bin/bash

##
## THIS IS A SIMPLE TEST AND PERFORMANCE TEST FOR WIO-UFS WITH THE GLOBAL CACHE
##

# NOTE STRACE DOES NOT WORK ON WIO-UFS WITH FORK OPTION AS MOUNT FUST IS GENERATING ERRORS THEN
# INSTEAD WE SHALL PROBABLY GET THE RESULTING PID AND ATTACH TO IT AS SOON AS WE CAN... OR USE -d DEBUG AND FOREGROUND AND THEN THINGS ARE FINE

trap "exit 1" TERM
export TOP_PID=$$

function fatal()
{
   echo "FATAL ERROR:$1" 1>&2
   kill -s TERM $TOP_PID
}



export PYRO_COMMTIMEOUT=3
#export PYRO_FLAME_ENABLED=1
export PYRO_SERIALIZER=pickle
export PYRO_SERIALIZERS_ACCEPTED=pickle,serpent,marshal,json

#Pyro4.config.COMMTIMEOUT=1.0
#Pyro4.config.SERIALIZER='pickle'


export S3QL_DISABLE_BACKEND_RETRY="1"
#EXTRA_OPT="--backend-options no-ssl"
EXTRA_OPT_DBG="--backend-options no-ssl"
WITH_STRACE=""
FULL_TEST=""
CONT="1"
TEST_DEBUG=""
BLOCK_SIZE="64k"
INCREMENT=8
MAXN=128
TEST_CONTAINER=rackspace://LON/WIO_test01/testfs 
QUICK=""
DUMMY=""
NT1=1
NT2=1

while [ "$CONT" ]; do
case "$1" in
  -d)
     TEST_DEBUG="1"
     shift 1
     ;;
  --dummy)
     DUMMY=1
     shift 1
     ;;
  -c)
     TEST_CONTAINER=$2
     shift 2
     ;;
  -q)
     QUICK="1"
     shift 1
     ;;     
  -f)
     FULL_TEST="1"
     shift 1
     ;;
  -t)
     WITH_STRACE="1"
     shift 1
     ;;     
  -b)
     BLOCK_SIZE=$2
     shift 2
     ;;
  -i)
     INCREMENT=$2
     shift 2
     ;;     
  -n)
     MAXN=$2
     shift 2
     ;;     
  -nt1)
     NT1=$2
     shift 2
     ;;     
  -nt2)
     NT2=$2
     shift 2
     ;;          
  -h)
     echo "-c <container> : container"
     echo "-d : debug"  
     echo "-q : quick (wio-ufs only)"       
     echo "-t : trace"       
     echo "-f : full test (including write)"
     echo "-h : this help message"
     exit 0
     ;;     
  *)
     CONT=""
     ;;
esac
done

echo CONT=$CONT 
echo FULL_TEST=$FULL_TEST
echo TEST_DEBUG=$TEST_DEBUG
echo WITH_STRACE=$WITH_STRACE

[ "$WITH_STRACE" ] && sudo /bin/sh -c 'echo "0" > /proc/sys/kernel/yama/ptrace_scope'

sleep 2

## --------------------------------------------------------------------------------------------------------------------------------

function update_s3ql() {
(cd ~/s3ql; sudo rm /usr/local/lib/python3.4/dist-packages/s3ql-2.11.1-py3.4-linux-x86_64.egg;sudo python3 setup.py install)
}


function restart_cached() {
ps fauxw | awk '/cached.s3ql/&&!/awk/{print $2;}' | xargs kill &> /dev/null
#;rm ~/.s3ql/wideio-mfcache/active-cache;
#(cached.s3ql || exit -1)&#sleep 1
[ "$TEST_DEBUG" ] || ((cached.s3ql $EXTRA_OPT & sleep 1) || fatal "failed to start cache")
[ "$TEST_DEBUG" ] && ((cached.s3ql --fg --debug  $EXTRA_OPT_DBG || fatal "failed to start cache")& sleep 1)
}

function start_cached() {
#ps fauxw | awk '/cached.s3ql/&&!/awk/{print $2;}' | xargs kill;rm ~/.s3ql/wideio-mfcache/active-cache; cached.s3ql --fg --debug &

#(cached.s3ql || exit -1)&sleep 1
[ "$TEST_DEBUG" ] || ((cached.s3ql $EXTRA_OPT & sleep 1) || fatal "failed to start cache")
[ "$TEST_DEBUG" ] && ((cached.s3ql --fg --debug  $EXTRA_OPT_DBG || fatal "failed to start cache")& sleep 1)
}

function start_cached_if_not_started() {
if [ "$(ps fauxw | awk '/cached.s3ql/&&!/awk/{print $2;}')" ] ; then
  start_cached
fi
}


## --------------------------------------------------------------------------------------------------------------------------------


TM1=test-mount1
TM2=test-mount2

#export WIOUFS_CACHEDIR="$HOME/wioufs-cache-test/"
#[ -d "$WIOUFS_CACHEDIR" ] || mkdir -p "$WIOUFS_CACHEDIR"

# ---------------------------------------------------------------------------------------------------------------------------------
[ "$(grep $TM1 /proc/mounts > /dev/null)" ] || fusermount -u $TM1
[ "$(grep $TM2 /proc/mounts > /dev/null)" ] || fusermount -u $TM2

echo "## UPDATING S3QL"
update_s3ql &> /dev/null

echo "## CLEANING CACHES"
#rm -rf ~/.s3ql-test

if [ "$FULL_TEST" ];then
rm -f pyro.log
rm -rf ~/.s3ql/*.db
rm -rf ~/.s3ql/*.params
rm -rf ~/.s3ql/*-cache
rm -f ~/.s3ql/wideio-mfcache/*
rm -f ~/.s3ql/auth.api.*.requestcache.pickle
rm ~/.auth.api.*.requestcache.pickle
fi

echo "## RESTARTING CACHED"
restart_cached
[ "$(ps fauxw | awk '/cached.s3ql/&&!/awk/{print $2;}')" ] || fatal "Cache crashed"


if [ "$FULL_TEST" ];then
echo "## CREATING TEST CONTENT" 
[ "$DUMMY" ] || mkfs.s3ql --plain --force $TEST_CONTAINER $EXTRA_OPT || fatal "mkfs error"

echo "## WRITTING TEST CONTENT"
[ -d "$TM1" ] || mkdir -p "$TM1"
echo "mount.s3ql $TEST_CONTAINER $TM1 "

if ! [ "$DUMMY" ]; then  
[ "$TEST_DEBUG" ] || (mount.s3ql $EXTRA_OPT $TEST_CONTAINER $TM1 || fatal "error with mount.s3ql")
[ "$TEST_DEBUG" ] && ((mount.s3ql --debug --fg $EXTRA_OPT_DBG $TEST_CONTAINER $TM1 & sleep 5) || fatal "error with mount.s3ql")

# --------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------
#mount.s3ql --debug --fg $TEST_CONTAINER $TM1 & sleep 1
sleep 1
[ "$(grep $TM1 /proc/mounts)" ] || fatal "assert failed: failed to mount $TM1"
fi

for ((i=1;i<$MAXN;i+=$INCREMENT)); do
  echo -n "$i," 1>&2
  dd if=/dev/urandom bs=$BLOCK_SIZE count=$i of=$TM1/tf-$i.dat
done 

echo "done" 1>&2

if ! [ "$DUMMY" ]; then  
fusermount -u $TM1
c=0
echo -n "## waiting for metadata upload to finish" 1>&2
while [ "$(ps fauxw| grep $TM1|grep -v grep)" ]; do
  echo -n "." 1>&2
  sleep 1
  c=$[$c+1]  
  if [ $c -gt 100 ]; then
    fatal "Unusually long... something is wront"
  fi  
done 
echo  "." 1>&2
fi

[ "$(grep $TM1 /proc/mounts)" ] && fatal "assert failed: failed to umount $TM1"
[ "$(grep $TM2 /proc/mounts)" ] && fatal "assert failed: failed to umount $TM2"


if ! [ "$DUMMY" ]; then  
echo "## --------------------------"
echo "## FSCK"
fsck.s3ql $EXTRA_OPT $TEST_CONTAINER
fi #< dummy
fi #< full

if ! [ "$QUICK" ]; then
echo "## --------------------------"
echo "## READING TEST CONTENT PASS0 (MOUNT.S3QL)"
[ -d "$TM1" ] || mkdir -p "$TM1"
if ! [ "$DUMMY" ]; then  
mount.s3ql $EXTRA_OPT $TEST_CONTAINER $TM1 || fatal "mount.s3ql error"
[ "$(grep $TM1 /proc/mounts)" ] || fatal "assert failed: failed to mount $TM1"
fi
(for ((i=1;i<$MAXN;i+=$INCREMENT)); do
  echo -n "$i," 1>&2
  dd if=$TM1/tf-$i.dat bs=$BLOCK_SIZE count=$i of=/dev/null
done ) 2>&1 | tee test-res-read-pass0a.txt
(for ((i=1;i<$MAXN;i+=$INCREMENT)); do
  echo -n "$i," 1>&2
  dd if=$TM1/tf-$i.dat bs=$BLOCK_SIZE count=$i of=/dev/null
done ) 2>&1 | tee test-res-read-pass0b.txt
echo "done" 1>&2

if ! [ "$DUMMY" ]; then  
fusermount -u $TM1
echo -n "## waiting for metadata upload to finish" 1>&2
c=0
while [ "$(ps fauxw| grep $TM1|grep -v grep)" ]; do
  echo -n "." 1>&2
  sleep 1
  c=$[$c+1]
  if [ $c -gt 100 ]; then
    fatal "Unusually long... something is wront"
  fi
done 
echo  "." 1>&2
fi

[ "$(grep $TM1 /proc/mounts)" ] && fatal "assert failed: failed to umount $TM1"
[ "$(grep $TM2 /proc/mounts)" ] && fatal "assert failed: failed to umount $TM2"


if ! [ "$DUMMY" ]; then  
echo "## --------------------------"
echo "## FSCK"
fsck.s3ql $EXTRA_OPT $TEST_CONTAINER
fi
fi

if [ "$DUMMY" ]; then  
exit 0
fi


echo "## --------------------------"
echo "## READING TEST CONTENT PASS1"
for ((nt1=0;nt1<$NT1;nt1++)); do
[ -d "$TM1" ] || mkdir -p "$TM1"
[ "$TEST_DEBUG" ] || (wio-ufs $TEST_CONTAINER $TM1 || wio-ufs $TEST_CONTAINER $TM1 --foreground --verbose || fatal "wio-ufs error")
[ "$TEST_DEBUG" ] && (wio-ufs $TEST_CONTAINER $TM1 --foreground --verbose & sleep 5)
[ "$(grep $TM1 /proc/mounts)" ] || fatal "assert failed: failed to mount $TM1"
[ "$WITH_STRACE" ] && (sleep 3;echo "startin strace -p $(ps fauxw|grep wio-ufs|grep $TM1|awk '{print $2;}')...";nohup strace -p $(ps fauxw|grep wio-ufs|grep $TM1|awk '{print $2;}') -c -T -o strace-pass-1-ufs.txt &)
(for ((i=1;i<$MAXN;i+=$INCREMENT)); do
  echo -n "$i," 1>&2
  dd if=$TM1/tf-$i.dat bs=$BLOCK_SIZE count=$i of=/dev/null
done )  2>&1 |tee test-res-read-pass1.txt
echo "done" 1>&2
if [ "$[$nt1+1]" != "$NT1" ]; then
fusermount -u $TM1
[ "$(grep $TM1 /proc/mounts > /dev/null)" ] && fatal "assert failed: failed to umount $TM1"
rm -rf ~/.s3ql/*.db
rm -rf ~/.s3ql/*.params
rm -rf ~/.s3ql/*-cache
rm -f ~/.s3ql/wideio-mfcache/*
rm -f ~/.s3ql/auth.api.*.requestcache.pickle
rm ~/.auth.api.*.requestcache.pickle
restart_cached
fi
done

echo "## --------------------------"
echo "## READING TEST CONTENT PASS2"
for ((nt2=0;nt2<$NT2;nt2++)); do
[ -d "$TM2" ] || mkdir -p "$TM2"
wio-ufs $TEST_CONTAINER $TM2 || wio-ufs $TEST_CONTAINER $TM2 --foreground --verbose || exit -1
[ "$(grep $TM2 /proc/mounts)" ] || fatal "assert failed: failed to mount $TM2 ."
[ "$(grep $TM1 /proc/mounts)" ] || fatal "assert failed: failed to mount $TM1 ."
[ "$WITH_STRACE" ] && (sleep 3; echo "starting strace -p $(ps fauxw|grep wio-ufs|grep $TM2|awk '{print $2;}').."; nohup strace -p $(ps fauxw|grep wio-ufs|grep $TM2|awk '{print $2;}') -c -T -o strace-pass-2-ufs.txt &)
(for ((i=1;i<$MAXN;i+=$INCREMENT)); do
  dd if=$TM2/tf-$i.dat bs=$BLOCK_SIZE count=$i of=/dev/null
done) 2>&1 | tee test-res-read-pass2.txt
if [ "$[$nt2+1]" != "$NT2" ]; then
fusermount -u $TM2
[ "$(grep $TM2 /proc/mounts > /dev/null)" ] && fatal "assert failed: failed to umount $TM2"
fi
done

fusermount -u $TM1
fusermount -u $TM2
[ "$(grep $TM1 /proc/mounts > /dev/null)" ] && fatal "assert failed: failed to umount $TM1"
[ "$(grep $TM2 /proc/mounts > /dev/null)" ] && fatal "assert failed: failed to umount $TM2"
# ---------------------------------------------------------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------------------------------------------

