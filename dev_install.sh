#!/usr/bin/env bash

if which apt-get &> /dev/null; then
  sudo apt-get install -y sshfs fuse libfuse-dev
  sudo apt-get install -y python3-setuptools
  sudo apt-get install -y python-setuptools 
  sudo apt-get install -y python3-pip
  sudo apt-get install -y python-pip 
  sudo apt-get install -y python3-dev
  sudo apt-get install -y libsqlite3-dev
  sudo apt-get install -y python3-apsw
  sudo apt-get install -y cython3
  sudo apt-get install -y ipython3
  sudo apt-get install -y python3-crypto
  sudo easy_install3 dugong
  sudo easy_install3 defusedxml
  sudo easy_install3 requests
  sudo easy_install3 fusepy
  sudo easy_install oslo.config 
  sudo apt-get install -y libattr1-dev 
  sudo easy_install oslo.utils
  sudo apt-get install -y python-apt
  sudo easy_install simplejson
  sudo apt-get install -y python-sphinx
  sudo apt-get install -y python3-sphinx
  sudo apt-get install -y pkg-config
  sudo apt-get install -y mercurial
fi    

function compile_llfuse() {
  hg clone https://code.google.com/p/python-llfuse/
  pushd python-llfuse
  python3 setup.py build_cython
  python3 setup.py build
  sudo python3 setup.py install
  popd
}


. bin/activate
echo "run setup..."

compile_llfuse

#[ -d lib/s3ql ] || (cd lib; hg clone ssh://hg@bitbucket.org/wideio/s3ql)
[ -d lib/s3ql ] || (cd lib; hg clone http://bitbucket.org/wideio/s3ql)
cd lib/s3ql &&   python3 setup.py build_cython  && python3 setup.py build_ext --inplace && sudo python3 setup.py install
