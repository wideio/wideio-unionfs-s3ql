#!/bin/bash

echo -n > stack_test.txt

for ((c=1; c<=256;c*=2)); do
  [ -d stack_test ] || mkdir -p stack_test
  echo "test with $c filesystems"
  SRC_FS_EXPR=""
  SEP=""  
  VROOT="/wideio/vroots/trusty"
  for ((f=0;f<$c;f++));do
    mkdir -p stack_test/fs-$f
    head --bytes=256k < /dev/urandom > stack_test/fs-$f/basefile-$f
    NF1=stack_test/fs-$f/$(md5sum < stack_test/fs-$f/basefile-$f | cut -c1-2,3-4,5-6,8-9,10-32 --output-delimiter "/")
    mkdir -p $(dirname "$NF1")
    echo $RANDOM > $NF1
    SRC_FS_EXPR="stack_test/fs-$f""$SEP""$SRC_FS_EXPR"
    SEP=","
  done
  
  [ -d stack_test/res_mount ] || mkdir -p stack_test/res_mount
  echo   wio-ufs $SRC_FS_EXPR stack_test/res_mount
  #wio-ufs $SRC_FS_EXPR stack_test/res_mount --foreground --verbose &  
  time wio-ufs $SRC_FS_EXPR stack_test/res_mount 2>&1 | grep real | cut -c "7-14" | tr -d '\n' >> stack_test.txt
  #sleep 1
  echo -n "$c" >> stack_test.txt    
  echo -n "," >> stack_test.txt    
  ##
  ## TEST DD-READ 1
  ##
  (time /bin/bash -c "
  for ((f=0;f<16;f++)); do
    f=[$RANDOM % c]
    dd if=\"stack_test/res_mount/basefile-$f\" of=/dev/null
  done
  "
  )  2>&1 | grep real | cut -c "7-14" | tr -d '\n' >> stack_test.txt
  echo -n "," >> stack_test.txt      
  ##
  ## TEST FIND 1
  ##
  (time find stack_test/res_mount > /dev/null) 2>&1 | grep real | cut -c "7-14" | tr -d '\n' >> stack_test.txt
  echo -n "," >> stack_test.txt      
  ##
  ## TEST FIND 2
  ##
  (time find stack_test/res_mount > /dev/null)  2>&1 | grep real | cut -c "7-14" | tr -d '\n' >> stack_test.txt
  echo -n "," >> stack_test.txt      
  echo "## unmounting"
  fusermount -u stack_test/res_mount
  [ "$(grep stack_test/res_mount /proc/mounts)" ] && exit -1
  
  echo "## remounting with vroot"
  wio-ufs $SRC_FS_EXPR,$VROOT stack_test/res_mount 
  
  ##
  ## TEST CHROOT LS
  ##  
  
  sudo chroot stack_test/res_mount /bin/bash -c 'time /bin/sh -c "ls" | awk "{}"' 2>&1 | grep real | cut -c "7-14" | tr -d '\n' >> stack_test.txt
  echo -n "," >> stack_test.txt    
  sudo chroot stack_test/res_mount /bin/bash -c 'time /bin/sh -c "ls" | awk "{}"' 2>&1 | grep real | cut -c "7-14" | tr -d '\n' >> stack_test.txt
  echo  >> stack_test.txt  
  
  echo "## unmounting"
  fusermount -u stack_test/res_mount
  [ "$(grep stack_test/res_mount /proc/mounts)" ] && exit -1
  
  rm -rf stack_test/fs-*
done

