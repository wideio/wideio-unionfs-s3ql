

INSTALL
=======


 * python3
 * (python3 module) llfuse
 * (python3 module) dugong
 * (python3 module) pycrypto
 * (python3 module) setuptools
 * (python3 module) fusepy
 * (python3 module) defusedxml
 * (python3 module) requests
 * (python3 module) aspw
 * (c library) libsqlite3-dev


RUN
---

 * go in lib/s3ql and run:
     python3 setup.py build_ext --inplace

now you will be able to use wideio-unionfs


VIRTUAL ENV
===========

install libsqlite3-dev library then run:

```sh
./setup.sh
. bin/activate
```

