#** About WIDE IO UNIONFS S3QL**


*  The **WIDE IO Unionfs S3QL** is the core of the WIDE IO Package System .It creates a unified file system and uses S3QL to store all its data online.In general, a unified filesystem is the concept  of mounting together other file systems. It allows files and directories of separate file systems, known as  branches, to be transparently overlaid, forming a single coherent file system. Contents of directories which have the same path within the merged branches will be seen together in a single merged directory, within the new, virtual filesystem.**WIDE IO Unionfs S3QL**  is such an implementation  adjusted to **WIDE IO** needs and system specifications. 

  
*  Version 0.2 

### How do I get set up? ###

* Firstly , you can clone this repository and then its dependencies. 
*   The dependencies are :
    *  [Compute Node Manager](https://bitbucket.org/wideio/wideio-compute-node-manager)
    *  [S3QL](https://bitbucket.org/wideio/S3QL)
    *  [Bridge](https://bitbucket.org/wideio/wideio-bridge-core)

   

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact