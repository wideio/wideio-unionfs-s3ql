#!/bin/bash
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
# 
# doinstall.sh 
# 
# Copyright (c) 2014 All Right Reserved, WIDE IO LTD, http://wide.io/
# ----------------------------------------------------------------------
# CONFIDENTIAL
# ----------------------------------------------------------------------
# THIS FILE IS NOT YET DISTRIBUTED AND SHALL NOT BE REPRODUCED UNLESS YOU
# HAVE RECEIVED A WRITTEN AND SIGNED AUTHORISATION TO DO SO.
# ----------------------------------------------------------------------
# THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
# KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
# PARTICULAR PURPOSE.
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################

##
## THIS SCRIPT SETS UP A COMPUTE NODE BY MAKING DEFAULT PROGRAMS AVAILABLE
## You need to set up  host_startup_script.sh separately in /etc/rc.local

echo ">  Creating symbolic links " 1>&2 



[ -d /wideio/bin ] || mkdir -p /wideio/bin
[ -d /wideio/lib ] || mkdir -p /wideio/lib
[ -d /wideio/logs ] || mkdir -p /wideio/logs
[ -d /wideio/lib/python ] || mkdir -p /wideio/lib/python
[ -d /wideio/etc ] || mkdir -p /wideio/etc
[ -d /wideio/etc/init ] || mkdir -p /wideio/etc/init
[ -d /wideio/buckets ] || mkdir -p /wideio/buckets
[ -L /wideio/env ]  || ln -s /wideio/etc/ /wideio/env


for f in *; do
  if (! [ "$(echo $f|grep '~')" ])  &&  [ -x "$f" ] && [ -f "$f" ]; then
    ln -sf $PWD/$f /wideio/bin
  fi
done

echo ">  Done ! " 1>&2 


