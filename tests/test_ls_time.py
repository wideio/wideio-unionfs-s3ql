#!/usr/bin/env python3
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
# 
# test_ls_time.py 
# 
# Copyright (c) 2014 All Right Reserved, WIDE IO LTD, http://wide.io/
# ----------------------------------------------------------------------
# CONFIDENTIAL
# ----------------------------------------------------------------------
# THIS FILE IS NOT YET DISTRIBUTED AND SHALL NOT BE REPRODUCED UNLESS YOU
# HAVE RECEIVED A WRITTEN AND SIGNED AUTHORISATION TO DO SO.
# ----------------------------------------------------------------------
# THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
# KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
# PARTICULAR PURPOSE.
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
"""
usage: ./exec mntpoint number_of_fs_in_mnt

"""
import timeit
import pickle
import os
import sh
import sys
import time

def pa(s):
    pass

if __name__ == "__main__":
    lsdf = "debug_timer/time_ls"
    lsd = {'first': [], 'second' : [], 'files_nb' : [], 'fs': []}
    if os.path.exists(lsdf) and os.path.isfile(lsdf):
        with open(lsdf, 'rb') as o:
            lsd = pickle.load(o)
            o.close()

    start = time.time()
    a = sh.ls('-l', sys.argv[1], _err=pa)
    lsd['first'].append(time.time() - start)
    start = time.time()
    a = sh.ls('-l', sys.argv[1], _err=pa)
    lsd['second'].append(time.time() - start)

    nb = int(sh.wc(a, '-l')) - 1
    lsd['files_nb'].append(nb)
    lsd['fs'].append(int(sys.argv[2]))
    with open(lsdf, 'wb') as o:
        pickle.dump(lsd, o)
        o.close()




