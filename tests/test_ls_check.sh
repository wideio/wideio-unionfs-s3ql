# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
# 
# test_ls_check.sh 
# 
# Copyright (c) 2014 All Right Reserved, WIDE IO LTD, http://wide.io/
# ----------------------------------------------------------------------
# CONFIDENTIAL
# ----------------------------------------------------------------------
# THIS FILE IS NOT YET DISTRIBUTED AND SHALL NOT BE REPRODUCED UNLESS YOU
# HAVE RECEIVED A WRITTEN AND SIGNED AUTHORISATION TO DO SO.
# ----------------------------------------------------------------------
# THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
# KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
# PARTICULAR PURPOSE.
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################

if [[ $1 && $2 && $3 && $4 ]] && [ -f "wio-ufs" ] && [ -d "$4" ]; then
    echo "start..."
else
    if [ -d "$4" ]; then
        echo "$4" exists
    fi
    echo -e "Usage:\n\t$0 [storage] [nameprefix] [counter] [mountpoint]"
    echo -e "\nexample:"
    echo -e "\t$0 'rackspace://LON/test/' 'name-prefix-' 100 'totopass'" 
    echo -e "\tthis will mount 100 fs with the name name-prefix-1 to name-prefix-100"
    exit
fi

if [ -f "./test_ls_time.py" ] && [ -f "./multi_mount.sh" ]; then
    echo -e "test_ls_time.py found, should work"
    echo -e "multi_mount.sh found, should work"
else
    echo -e "no ls_time.py or mount.sh found, exit"
    exit
fi

storage="$1"
prefix="$2"
mnt="$4"
let count="$3"

# the first one failed for now (0)
let i=0
let res=0
while [[ $i -le $count ]]; do

    let old=$res
    res=`echo "2^$i" | bc`
    let res=$res
    if [[ $res != "$old" ]];then
        echo mount ... $res
        echo "./multi_mount.sh $storage $prefix $res $mnt"
        ./multi_mount.sh $storage $prefix $res $mnt
        sleep 1
        ./test_ls_time.py $mnt $res
        fusermount -u $mnt
        sleep 1
    fi

    let old=$res
    res=`echo "2^$i + 2^($i-1)" | bc `
    let res=$res
    if [[ $res != "$old" ]];then
        echo mount ... $res
        echo "./multi_mount.sh $storage $prefix $res $mnt"
        ./multi_mount.sh $storage $prefix $res $mnt
        sleep 1
        ./test_ls_time.py $mnt $res
        fusermount -u $mnt
        sleep 1
    fi

    if [ $res -gt $count ];then
        let i=$count
    else
        let i+=1
    fi
done
