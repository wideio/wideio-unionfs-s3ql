#!/usr/bin/env bash
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
# 
# multi_makefs.sh 
# 
# Copyright (c) 2014 All Right Reserved, WIDE IO LTD, http://wide.io/
# ----------------------------------------------------------------------
# CONFIDENTIAL
# ----------------------------------------------------------------------
# THIS FILE IS NOT YET DISTRIBUTED AND SHALL NOT BE REPRODUCED UNLESS YOU
# HAVE RECEIVED A WRITTEN AND SIGNED AUTHORISATION TO DO SO.
# ----------------------------------------------------------------------
# THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
# KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
# PARTICULAR PURPOSE.
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################

if [[ "$1" && "$2" && "$3" && "$4" ]] && [ -f "../lib/s3ql/bin/mkfs.s3ql" ]; then
    echo "start..."
else
    echo -e "Usage:\n\t$0 [storage] [nameprefix] [counter] [passphrase]"
    echo -e "\nexample:"
    echo -e "\t$0 'rackspace://LON/test/' 'name-prefix-' 100 'totopass'" 
    echo -e "\tthis will make 100 fs with the name name-prefix-1 to name-prefix-100"
    echo -e "\nrequired:"
    echo -e "\tpython3"
    echo -e "\t../lib/s3ql/bin/mkfs.s3ql"
    exit
fi

let count=$3
let i=0

cd lib/s3ql/bin/
while [[ $i -le $count ]]; do
    ../lib/s3ql/bin/mkfs.s3ql "$1$2$i" --passphrase "$4"
    let i+=1
done
