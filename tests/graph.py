#!/usr/bin/env python3
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
# 
# graph.py 
# 
# Copyright (c) 2014 All Right Reserved, WIDE IO LTD, http://wide.io/
# ----------------------------------------------------------------------
# CONFIDENTIAL
# ----------------------------------------------------------------------
# THIS FILE IS NOT YET DISTRIBUTED AND SHALL NOT BE REPRODUCED UNLESS YOU
# HAVE RECEIVED A WRITTEN AND SIGNED AUTHORISATION TO DO SO.
# ----------------------------------------------------------------------
# THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
# KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
# PARTICULAR PURPOSE.
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
import pickle
import sys
import argparse
import matplotlib.pyplot as plt


def load_args(args):
    opt = argparse.ArgumentParser()
    opt.add_argument('file1', metavar='<file>', help='file op needed')
    opt.add_argument("file2", metavar='<file>', help="data file pickle db")
    opt.add_argument("--each", action='store_true', default=False,
                     help="data file pickle db")
    opt.add_argument("--moyen", action='store_true', default=False,
                     help="data file pickle db")
    opt.add_argument("--variance", action='store_true', default=False,
                     help="data file pickle db")
    opt = opt.parse_args(args)
    return opt


def each_mountcourbe(l):
    nl = []
    nl.append(l[0])
    lenght = len(l)
    i = 1
    while lenght > i:
        nl.append(l[i] - l[i - 1])
        i += 1
    return nl


def moyen_mountcourbe(courbe, rng=None):
    courbe = sum(courbe) / len(courbe)
    courbe = [courbe, courbe]
    if rng is not None:
        rng = [rng[0], rng[-1]]
        return (courbe, rng)
    return courbe


def variance(l, rng=None):
    m = moyen_mountcourbe(l)
    m = m[0]
    m = moyen_mountcourbe([(x-m)**2 for x in l])[0]
    print("variance:", m)
    if rng is not None:
        rng = [rng[0], rng[-1]]
        return ([m, m], rng)
    return [m, m]


def main(args):
    options = load_args(args)
    with open(options.file1, 'rb') as o:
        courbe1 = pickle.load(o)
    with open(options.file2, 'rb') as o:
        courbe2 = pickle.load(o)
    rng1 = range(1, len(courbe1) + 1)
    rng2 = range(1, len(courbe2) + 1)
    if options.each:
        courbe1 = each_mountcourbe(courbe1)
        courbe2 = each_mountcourbe(courbe2)
        if options.moyen:
            courbe1, rng1 = moyen_mountcourbe(courbe1, rng1)
            courbe2, rng2 = moyen_mountcourbe(courbe2, rng2)
        elif options.variance:
            courbe1, rng1 = variance(courbe1, rng1)
            courbe2, rng2 = variance(courbe2, rng2)
    plt.plot(rng1, courbe1)
    plt.plot(rng2, courbe2)
    plt.show()

if __name__ == '__main__':
    main(sys.argv[1:])
    pass
