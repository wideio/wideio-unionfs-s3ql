# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
# 
# multi_mount.sh 
# 
# Copyright (c) 2014 All Right Reserved, WIDE IO LTD, http://wide.io/
# ----------------------------------------------------------------------
# CONFIDENTIAL
# ----------------------------------------------------------------------
# THIS FILE IS NOT YET DISTRIBUTED AND SHALL NOT BE REPRODUCED UNLESS YOU
# HAVE RECEIVED A WRITTEN AND SIGNED AUTHORISATION TO DO SO.
# ----------------------------------------------------------------------
# THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
# KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
# PARTICULAR PURPOSE.
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################

if [[ "$1" && "$2" && "$3" && "$4" ]] && [ -f "../wio-ufs" ] && [ -d "$4" ]; then
    echo "start..."
else
    echo -e "Usage:\n\t$0 [storage] [nameprefix] [counter] [mountpoint]"
    echo -e "\nexample:"
    echo -e "\t$0 'rackspace://LON/test/' 'name-prefix-' 100 'totopass'" 
    echo -e "\tthis will mount 100 fs with the name name-prefix-1 to name-prefix-100"
    exit
fi

storage="$1"
prefix="$2"
mnt="$4"
let count=$3
let i=0

echo ./wio-ufs "$storage$prefix$i-$count" $mnt

if [[ "$3" == "1" ]];then
    ../wio-ufs "$storage$prefix$i" $mnt
    exit
fi  

total="'"
while [[ $i -le $count ]]; do
    total+="$storage$prefix$i,"
    let i+=1
done
total+="'"

echo "starting..."


echo $total
PYTHONPATH="$PWD/../lib/s3ql/src:$PYTHONPATH" ../wio-ufs $total $mnt 
