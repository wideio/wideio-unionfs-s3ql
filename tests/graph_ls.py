#!/usr/bin/env python3
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
# 
# graph_ls.py 
# 
# Copyright (c) 2014 All Right Reserved, WIDE IO LTD, http://wide.io/
# ----------------------------------------------------------------------
# CONFIDENTIAL
# ----------------------------------------------------------------------
# THIS FILE IS NOT YET DISTRIBUTED AND SHALL NOT BE REPRODUCED UNLESS YOU
# HAVE RECEIVED A WRITTEN AND SIGNED AUTHORISATION TO DO SO.
# ----------------------------------------------------------------------
# THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
# KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
# PARTICULAR PURPOSE.
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
import pickle
import sys
import argparse
import matplotlib.pyplot as plt


def load_args(args):
    opt = argparse.ArgumentParser()
    opt.add_argument('file', metavar='<pickle_file>',
                     help='pickle file to load data')
    opt = opt.parse_args(args)
    return opt


def each_mountcourbe(l):
    nl = []
    nl.append(l[0])
    lenght = len(l)
    i = 1
    while lenght > i:
        nl.append(l[i] - l[i - 1])
        i += 1
    return nl


def main(args):
    options = load_args(args)
    with open(options.file, 'rb') as o:
        data = pickle.load(o)
        o.close()
    fs = data['fs']
    f = data['first']
    s = data['second']
    i = 0
    while len(fs) > i:
        print(fs[i], ": f[%s] s[%s]" % (f[i], s[i]))
        i += 1
    plt.plot(data['fs'], data['first'])
    plt.plot(data['fs'], data['second'])
    plt.ylabel('times (sec)')
    plt.xlabel('fs mounted (1300~ files for each)')
    plt.show()

if __name__ == '__main__':
    main(sys.argv[1:])
    pass
